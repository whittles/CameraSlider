int Pin2 = 5; //Pin 1 cannot be used for this. Start at 2.
int Pin3 = 4;
int Pin4 = 3;
int Pin5 = 2;
int Pin8 = 8;
int Pin9 = 9;

//Switch Status Pins
int Pin6 = 6; //Pin2 Status
int Pin7 = 7; //Pin3 Status
int Pin10 = 10; //Pin4 Status
int Pin11 = 11; //Pin5 Status

int SliderLength = 762; //Travel length in mm 1050 orig
int MoveStepDelayMicroSec = 500;
float Delay_a = 0.001; // replacing hardcoded .0008. This is to be 2x the microsecond delay on the movestepper func but in Ms not micro
int Delay_b = 1000; // replacing hardcoded 1000 in step delay formulas

double TravelTime; //Used for calculations in the loop section.
double Steps = 3880; //Steps required to move along the entire Rail.200 * (SliderLength / (3.14 * PulleyDiameter))  || 10695 orig and 400 original resolution
double StepDelay;
int StepsTaken = 0;

int SendHomeDelay = 5; //Seconds before sending home with all switches off

bool s2;   ///s2 stands for switch2_status. It starts at 2 so we can match the pin.
bool s3;
bool s4;
bool s5;

int t1 = 120; // 1    1 min
int t2 = 180; // 2    2 min
int t3 = 240; // 3    3 min
int t4 = 300; // 4    4 min
int t5 = 360; // 1,2  5 min
int t6 = 600; // 1,3  10 min
int t7 = 1200; // 1,4  20 min
int t8 = 1800; // 2,3  30 min
int t9 = 2400; // 2,4  40 min
int t10 = 3600; // 3,4  1 hr
int t11 = 5400; // 1,2,3  1.5hr
int t12 = 7200; // 2,3,4  2 hr
int t13 = 14400; // 1,3,4  4 hr
int t14 = 28800; // 1,2,4  8 hr
int t15 = 43200; // 1,2,3,4  12 hr


const int stepPin = 8; //defines the step pin
const int dirPin = 9; //defines the direction pin
int dirSet;
int dirMode = 1;
//int mode;

void setup()
{
  Serial.begin(9600); //Required for Serial Monitor
  pinMode(Pin2, INPUT);
  pinMode(Pin3, INPUT);
  pinMode(Pin4, INPUT);
  pinMode(Pin5, INPUT);

  pinMode(stepPin, OUTPUT);
  pinMode(dirPin, OUTPUT);
  Serial.println("");
  Serial.print("Steps = ");
  Serial.println(Steps);
  digitalWrite(dirPin, HIGH);  //Defines the direction of travel. LOW reverses it.
  dirSet = 1;
  
  //Status Pins
  pinMode(Pin6, OUTPUT);
  pinMode(Pin7, OUTPUT);
  pinMode(Pin10, OUTPUT);
  pinMode(Pin11, OUTPUT);
  
  digitalWrite(Pin6, LOW);
  digitalWrite(Pin7, LOW);
  digitalWrite(Pin10, LOW);
  digitalWrite(Pin11, LOW);
}

void MoveStepper(int mode=1)
{
  digitalWrite(stepPin, HIGH); //This moves the stepper by 1 step.
  delayMicroseconds(MoveStepDelayMicroSec); //Depends on Motor 500-1000 is good
  digitalWrite(stepPin, LOW);
  delayMicroseconds(MoveStepDelayMicroSec);
  if(mode==1)
  {
    StepsTaken++;
  } else {
    StepsTaken--;
  }
  Serial.print("Steps Taken: ");
  Serial.println(StepsTaken);
  CheckDirection();
}

void UpdateStatus()
{
    if(s2 == LOW)
    {
        digitalWrite(Pin6, HIGH);
    } else if (s2 == HIGH)
    {
        digitalWrite(Pin6, LOW);
    }
    
    if(s3 == LOW)
    {
        digitalWrite(Pin7, HIGH);
    } else if (s3 == HIGH)
    {
        digitalWrite(Pin7, LOW);
    }
    
    if(s4 == LOW)
    {
        digitalWrite(Pin10, HIGH);
    } else if (s4 == HIGH)
    {
        digitalWrite(Pin10, LOW);
    }
    
    if(s5 == LOW)
    {
        digitalWrite(Pin11, HIGH);
    } else if (s5 == HIGH)
    {
        digitalWrite(Pin11, LOW);
    }
    
}

void CheckDirection()
{
   //Reverse the direction of the slider at the end.
  if (StepsTaken >= Steps)
  {
    //delay (1000);
    ChangeDirection();
    StepsTaken = 0;
  } 
}

void ChangeDirection()
{
  if (dirSet == 1)
    {
      digitalWrite(dirPin, LOW);
      dirSet = 0;
    }
    else if (dirSet == 0)
    {
      digitalWrite(dirPin, HIGH);
      dirSet = 1;
    }
}

void SendHome()
{
  delay(SendHomeDelay * 1000);
  boolean SwitchedOver = false;
  while((s2 == HIGH) && (s3 == HIGH) && (s4 == HIGH) && (s5 == HIGH) && StepsTaken > 0)
  {
    if(SwitchedOver == false)
    {
      if(dirSet == 1)
      {
         ChangeDirection();
         dirMode = 0;
      } else {
        StepsTaken = Steps - StepsTaken;
      }
      SwitchedOver = true;
    }
    Serial.println("Mode = Reset");
    TravelTime = t1; //The slider will move the distance in x seconds.enter 5
    StepDelay = ((TravelTime / Steps) - Delay_a)*Delay_b; //This calculates the delay required in ms between steps to make the journey in the desired amount of time. 0.001 Seconds are deducted because the MoveStepper Method includes 2x 500microseconds delays.
    MoveStepper(dirMode);
    Serial.print("Stepdelay = ");
    Serial.println(StepDelay);
    Serial.println("DirMode = ");
    Serial.println(dirMode);
    delay (StepDelay);
  }
  if(SwitchedOver == true)
  {
    ChangeDirection();
  }
// THis is in miliseconds (ms) Not MICRO!
}
// the loop routine runs over and over again forever:
void loop()
{
  s2 = digitalRead(Pin2);  ///This checks the status of the switches.
  s3 = digitalRead(Pin3);
  s4 = digitalRead(Pin4);
  s5 = digitalRead(Pin5);
  UpdateStatus();
  
  // Check if it should reset
  if ((s2 == HIGH) && (s3 == HIGH) && (s4 == HIGH) && (s5 == HIGH)) ///LOW means switch is pressed/closed/conductive.
  {
    if (StepsTaken < Steps && StepsTaken > 0)
    { 
      SendHome();
    }
  }
  
  //Time controls
  if ((s2 == LOW) && (s3 == HIGH) && (s4 == HIGH) && (s5 == HIGH)) ///LOW means switch is pressed/closed/conductive.
  {
    if (StepsTaken < Steps)
    { Serial.println("Mode = s2");
      TravelTime = t1; //The slider will move the distance in x seconds.enter 5
      StepDelay = ((TravelTime / Steps) - Delay_a)*Delay_b; //This calculates the delay required in ms between steps to make the journey in the desired amount of time. 0.001 Seconds are deducted because the MoveStepper Method includes 2x 500microseconds delays.
      MoveStepper(1);
      Serial.print("Stepdelay = ");
      Serial.println(StepDelay);
      delay (StepDelay);
      // THis is in miliseconds (ms) Not MICRO!

    }
  }


  if ((s2 == HIGH) && (s3 == LOW) &&  (s4 == HIGH) && (s5 == HIGH)) ///LOW means switch is pressed/closed/conductive.
  {
    if (StepsTaken < Steps)
    { Serial.println("Mode = s3");
      TravelTime = t2; //The slider will move the distance in x seconds.enter 5
      StepDelay = ((TravelTime / Steps) - Delay_a)*Delay_b; //This calculates the delay required in ms between steps to make the journey in the desired amount of time. 0.001 Seconds are deducted because the MoveStepper Method includes 2x 500microseconds delays.
      MoveStepper(1);
      Serial.print("Stepdelay = ");
      Serial.println(StepDelay);
      delay (StepDelay);
      // THis is in miliseconds (ms) Not MICRO!

    }
  }
  if ((s2 == HIGH) && (s3 == HIGH) && (s4 == LOW) && (s5 == HIGH)) ///LOW means switch is pressed/closed/conductive.
  {
    if (StepsTaken < Steps)
    { Serial.println("Mode = s4");
      TravelTime = t3; //The slider will move the distance in x seconds.enter 5
      StepDelay = ((TravelTime / Steps) - Delay_a)*Delay_b; //This calculates the delay required in ms between steps to make the journey in the desired amount of time. 0.001 Seconds are deducted because the MoveStepper Method includes 2x 500microseconds delays.
      MoveStepper();
      Serial.print("Stepdelay = ");
      Serial.println(StepDelay);
      delay (StepDelay);
      // THis is in miliseconds (ms) Not MICRO!

    }
  }
  if ((s2 == HIGH) && (s3 == HIGH) && (s4 == HIGH) && (s5 == LOW)) ///LOW means switch is pressed/closed/conductive.
  {
    if (StepsTaken < Steps)
    { Serial.println("Mode = s5");
      TravelTime = t4; //The slider will move the distance in x seconds.enter 5
      StepDelay = ((TravelTime / Steps) - Delay_a)*Delay_b; //This calculates the delay required in ms between steps to make the journey in the desired amount of time. 0.001 Seconds are deducted because the MoveStepper Method includes 2x 500microseconds delays.
      MoveStepper();
      Serial.print("Stepdelay = ");
      Serial.println(StepDelay);
      delay (StepDelay);
      // THis is in miliseconds (ms) Not MICRO!

    }
  }
  if ((s2 == LOW) && (s3 == LOW) && (s4 == HIGH) && (s5 == HIGH)) ///LOW means switch is pressed/closed/conductive.
  {
    if (StepsTaken < Steps)
   { Serial.println("Mode = s2,3");
      TravelTime = t5; //The slider will move the distance in x seconds.enter 5
      StepDelay = ((TravelTime / Steps) - Delay_a)*Delay_b; //This calculates the delay required in ms between steps to make the journey in the desired amount of time. 0.001 Seconds are deducted because the MoveStepper Method includes 2x 500microseconds delays.
      MoveStepper();
      Serial.print("Stepdelay = ");
      Serial.println(StepDelay);
      delay (StepDelay);
      // THis is in miliseconds (ms) Not MICRO!

    }
  }
  if ((s2 == LOW) && (s3 == HIGH) && (s4 == LOW) && (s5 == HIGH)) ///LOW means switch is pressed/closed/conductive.
  {
    if (StepsTaken < Steps)
    { Serial.println("Mode = s2,4");
      TravelTime = t6; //The slider will move the distance in x seconds.enter 5
      StepDelay = ((TravelTime / Steps) - Delay_a)*Delay_b; //This calculates the delay required in ms between steps to make the journey in the desired amount of time. 0.001 Seconds are deducted because the MoveStepper Method includes 2x 500microseconds delays.
      MoveStepper();
      Serial.print("Stepdelay = ");
      Serial.println(StepDelay);
      delay (StepDelay);
      // THis is in miliseconds (ms) Not MICRO!

    }

  }
  if ((s2 == LOW) && (s3 == HIGH) && (s4 == HIGH)  && (s5 == LOW)) ///LOW means switch is pressed/closed/conductive.
  {
    if (StepsTaken < Steps)
    { Serial.println("Mode = s2,5");
      TravelTime = t7; //The slider will move the distance in 30s
      StepDelay = ((TravelTime / Steps) - Delay_a) * Delay_b; //This calculates the delay required in ms between steps to make the journey in the desired amount of time. 0.001 Seconds are deducted because the MoveStepper Method includes 2x 500microseconds delays.
      MoveStepper();
      Serial.print("Stepdelay = ");
      Serial.println(StepDelay);
      delay (StepDelay); // THis is in miliseconds (ms) Not MICRO!
    }

  }
  if ((s2 == HIGH) && (s3 == LOW) && (s4 == LOW) && (s5 == HIGH)) ///LOW means switch is pressed/closed/conductive.
  {
    if (StepsTaken < Steps)
    { Serial.println("Mode = s3,4");
      TravelTime = t8; //The slider will move the distance in 30s
      StepDelay = ((TravelTime / Steps) - Delay_a) * Delay_b; //This calculates the delay required in ms between steps to make the journey in the desired amount of time. 0.001 Seconds are deducted because the MoveStepper Method includes 2x 500microseconds delays.
      MoveStepper();
      Serial.print("Stepdelay = ");
      Serial.println(StepDelay);
      delay (StepDelay); // THis is in miliseconds (ms) Not MICRO!
    }

  }
  if ((s2 == HIGH) && (s3 == LOW) && (s4 == HIGH) && (s5 == LOW)) ///LOW means switch is pressed/closed/conductive.
  {
    if (StepsTaken < Steps)
    { Serial.println("Mode = s3,5");
      TravelTime = t9; //The slider will move the distance in 30s
      StepDelay = ((TravelTime / Steps) - Delay_a) * Delay_b; //This calculates the delay required in ms between steps to make the journey in the desired amount of time. 0.001 Seconds are deducted because the MoveStepper Method includes 2x 500microseconds delays.
      MoveStepper();
      Serial.print("Stepdelay = ");
      Serial.println(StepDelay);
      delay (StepDelay); // THis is in miliseconds (ms) Not MICRO!
    }

  }
  if ((s2 == HIGH) && (s3 == HIGH) && (s4 == LOW) && (s5 == LOW)) ///LOW means switch is pressed/closed/conductive.
  {
    if (StepsTaken < Steps)
    { Serial.println("Mode = s4,5");
      TravelTime = t10; //The slider will move the distance in 30s
      StepDelay = ((TravelTime / Steps) - Delay_a) * Delay_b; //This calculates the delay required in ms between steps to make the journey in the desired amount of time. 0.001 Seconds are deducted because the MoveStepper Method includes 2x 500microseconds delays.
      MoveStepper();
      Serial.print("Stepdelay = ");
      Serial.println(StepDelay);
      delay (StepDelay); // THis is in miliseconds (ms) Not MICRO!
    }

  }
  if ((s2 == LOW) && (s3 == LOW) && (s4 == LOW) && (s5 == HIGH)) ///LOW means switch is pressed/closed/conductive.
  {
    if (StepsTaken < Steps)
    { Serial.println("Mode = s2,3,4");
      TravelTime = t11; //The slider will move the distance in 30s
      StepDelay = ((TravelTime / Steps) - Delay_a) * Delay_b; //This calculates the delay required in ms between steps to make the journey in the desired amount of time. 0.001 Seconds are deducted because the MoveStepper Method includes 2x 500microseconds delays.
      MoveStepper();
      Serial.print("Stepdelay = ");
      Serial.println(StepDelay);
      delay (StepDelay); // THis is in miliseconds (ms) Not MICRO!
    }

  }
  if ((s2 == HIGH) && (s3 == LOW) && (s4 == LOW) && (s5 == LOW)) ///LOW means switch is pressed/closed/conductive.
  {
    if (StepsTaken < Steps)
    { Serial.println("Mode = s3,4,5");
      TravelTime = t12; //The slider will move the distance in 30s
      StepDelay = ((TravelTime / Steps) - Delay_a) * Delay_b; //This calculates the delay required in ms between steps to make the journey in the desired amount of time. 0.001 Seconds are deducted because the MoveStepper Method includes 2x 500microseconds delays.
      MoveStepper();
      Serial.print("Stepdelay = ");
      Serial.println(StepDelay);
      delay (StepDelay); // THis is in miliseconds (ms) Not MICRO!
    }

  }
  if ((s2 == LOW) && (s3 == HIGH) && (s4 == LOW) && (s5 == LOW)) ///LOW means switch is pressed/closed/conductive.
  {
    if (StepsTaken < Steps)
    { Serial.println("Mode = s2,4,5");
      TravelTime = t13; //The slider will move the distance in 30s
      StepDelay = ((TravelTime / Steps) - Delay_a) * Delay_b; //This calculates the delay required in ms between steps to make the journey in the desired amount of time. 0.001 Seconds are deducted because the MoveStepper Method includes 2x 500microseconds delays.
      MoveStepper();
      Serial.print("Stepdelay = ");
      Serial.println(StepDelay);
      delay (StepDelay); // THis is in miliseconds (ms) Not MICRO!
    }

  }
  if ((s2 == LOW) && (s3 == LOW) && (s4 == HIGH) && (s5 == LOW)) ///LOW means switch is pressed/closed/conductive.
  {
    if (StepsTaken < Steps)
    { Serial.println("Mode = s2,3,5");
      TravelTime = t14; //The slider will move the distance in 30s
      StepDelay = ((TravelTime / Steps) - Delay_a) * Delay_b; //This calculates the delay required in ms between steps to make the journey in the desired amount of time. 0.001 Seconds are deducted because the MoveStepper Method includes 2x 500microseconds delays.
      MoveStepper();
      Serial.print("Stepdelay = ");
      Serial.println(StepDelay);
      delay (StepDelay); // THis is in miliseconds (ms) Not MICRO!
    }

  }
  if ((s2 == LOW) && (s3 == LOW) && (s4 == LOW) & (s5 == LOW)) ///LOW means switch is pressed/closed/conductive.
  {
    if (StepsTaken < Steps)
    { Serial.println("Mode = s2,3,4,5");
      TravelTime = t15; //The slider will move the distance in 30s
      StepDelay = ((TravelTime / Steps) - Delay_a) * Delay_b; //This calculates the delay required in ms between steps to make the journey in the desired amount of time. 0.001 Seconds are deducted because the MoveStepper Method includes 2x 500microseconds delays.
      MoveStepper();
      Serial.print("Stepdelay = ");
      Serial.println(StepDelay);
      delay (StepDelay); // THis is in miliseconds (ms) Not MICRO!
    }

  }
}
