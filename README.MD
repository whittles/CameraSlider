Arduino code to drive the camera slider.

Original source: https://fstoppers.com/diy/build-your-own-motorized-camera-slider-168165

Added auto reverse functionality.